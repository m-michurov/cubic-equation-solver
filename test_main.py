from typing import Union, Tuple
from unittest import TestCase

from main import localize, solve_bisect, solve_quadratic, solve_cubic


def get_abc(x1: float,
            x2: Union[float, complex],
            x3: Union[float, complex]) -> Tuple[float, float, float]:
    if type(x2) is complex and type(x3) is not complex or type(x2) is not complex and type(x3) is complex:
        raise TypeError('x2 ({}, {}) and x3 ({}, {}) must have same type (both float or both complex)'.format(
            x2, type(x2), x3, type(x3)
        ))

    if type(x2) is complex and x2 != x3.conjugate():
        raise ValueError('if x2 is complex, it must be conjugate of x3')

    a = -(x1 + x2 + x3)
    b = x1 * x2 + x1 * x3 + x2 * x3
    c = -x1 * x2 * x3

    if type(x2) is complex:
        a = a.real
        b = b.real
        c = c.real

    return float(a), float(b), float(c)


class Test(TestCase):
    def test_localize(self):
        test_cases = [
            (lambda x: x - 10, 0, 1, (8, 16)),
            (lambda x: x - 10, 0, 20, (0, 20)),
            (lambda x: x ** 2 - 100, 0, 1, (8, 16)),
            (lambda x: x ** 2 - 100, 0, 5, (5, 10)),
            (lambda x: x ** 2 - 100, 0, -1, (-16, -8)),
            (lambda x: x ** 2 - 100, 0, -5, (-10, -5)),
            (lambda x: -x ** 2 + 1_000_000, 0, 2, (512, 1024)),
            (lambda x: -x ** 2 + 1_000_000, 0, -2, (-1024, -512)),
            (lambda x: (x - 565) * (x + 71), 0, 1, (512, 1024)),
            (lambda x: (x - 565) * (x + 71), 0, -1, (-128, -64)),
            (lambda x: (x + 565) * (x - 71), 0, 1, (64, 128)),
            (lambda x: (x + 565) * (x - 71), 0, -1, (-1024, -512))
        ]

        for case in test_cases:
            fun, a, delta, want_interval = case
            got_interval = localize(fun, a, delta)
            self.assertEqual(want_interval, got_interval, 'wrong answer')

    def test_solve_bisect(self):
        eps = 1e-6
        test_cases = [
            (lambda x: x - 10, (8, 16)),
            (lambda x: x - 10, (0, 20)),
            (lambda x: x ** 2 - 100, (8, 16)),
            (lambda x: x ** 2 - 100, (5, 10)),
            (lambda x: x ** 2 - 100, (-16, -8)),
            (lambda x: x ** 2 - 100, (-10, -5)),
            (lambda x: -x ** 2 + 1_000_000, (512, 1024)),
            (lambda x: -x ** 2 + 1_000_000, (-1024, -512)),
            (lambda x: (x - 565) * (x + 71), (512, 1024)),
            (lambda x: (x - 565) * (x + 71), (-128, -64)),
            (lambda x: (x + 565) * (x - 71), (64, 128)),
            (lambda x: (x + 565) * (x - 71), (-1024, -512))
        ]

        for case in test_cases:
            fun, search_interval = case
            got_root = solve_bisect(fun, *search_interval, eps)
            self.assertLessEqual(abs(fun(got_root)), eps, 'wrong answer')

    def test_solve_quadratic(self):
        eps = 1e-6
        test_cases = [
            ((-1, 5), 1, -4, -5),
            ((-1, 5), 5, -4 * 5, -5 * 5),
            ((-1, 5), 100, -4 * 100, -5 * 100),
            ((-1, 5), 1e-5, -4 * 1e-5, -5 * 1e-5),
            ((-1_000, 5_000), 1, -4_000, -5_000_000),
            ((-1_000, 5_000), 5, -4_000 * 5, -5_000_000 * 5),
            ((-1_000, 5_000), 100, -4_000 * 100, -5_000_000 * 100),
            ((-1_000, 5_000), 1e-5, -4_000 * 1e-5, -5_000_000 * 1e-5),
            ((2, 10_000), 1, -10_002, 20_000),
            ((2, 10_000), 5, -10_002 * 5, 20_000 * 5),
            ((2, 10_000), 100, -10_002 * 100, 20_000 * 100),
            ((2, 10_000), 1e-5, -10_002 * 1e-5, 20_000 * 1e-5),
            (None, 1, 1, 1),
            (None, 5_000, 7_777, 9_999),
            (None, -1, -2, -3),
            ((1_000,), 1, -2_000, 1_000_000),
            ((1_000,), 5, -2_000 * 5, 1_000_000 * 5),
            ((1_000,), 100, -2_000 * 100, 1_000_000 * 100),
            ((1_000,), 1e-5, -2_000 * 1e-5, 1_000_000 * 1e-5),
            ((5_000,), 1, -10_000, 25_000_000),
            ((5_000,), 5, -10_000 * 5, 25_000_000 * 5),
            ((5_000,), 100, -10_000 * 100, 25_000_000 * 100),
            ((5_000,), 1e-5, -10_000 * 1e-5, 25_000_000 * 1e-5),
            ((-5_000,), 1, 10_000, 25_000_000),
            ((-5_000,), 5, 10_000 * 5, 25_000_000 * 5),
            ((-5_000,), 100, 10_000 * 100, 25_000_000 * 100),
            ((-5_000,), 1e-5, 10_000 * 1e-5, 25_000_000 * 1e-5),
        ]

        for case in test_cases:
            roots, a, b, c = case
            got_roots = solve_quadratic(a, b, c)
            if not roots:
                self.assertIsNone(got_roots)
            else:
                self.assertEqual(len(roots), len(got_roots))
                for i, root in enumerate(roots):
                    self.assertAlmostEqual(root, got_roots[i], msg='wrong answer', delta=eps)

    def test_get_abc(self):
        test_cases = [
            (1, 2, 3),
            (1, 1j, -1j),
            (1, 2 + 3j, 2 - 3j),
            (2, 3, 4),
            (1000001, 10000002, 10000003)
        ]

        for case in test_cases:
            a, b, c = get_abc(*case)
            print(a, b, c)
            self.assertTrue(type(a) is float)
            self.assertTrue(type(b) is float)
            self.assertTrue(type(c) is float)

    def test_solve_cubic(self):
        test_cases = [
            ((1, 2, 3), 1e-5, 1),
            ((1, 2, 2), 1e-5, 1),
            ((2, 2, 2), 1e-5, 1),
            ((5, 2j, -2j), 1e-5, 1),
            ((-6845, 2 + 2j, 2 - 2j), 1e-5, 1),
            ((-10, 2, 60), 1e-5, 1),
            ((-555, 9758, 100000), 1e-5, 1),
            ((267, 997 + 6345j, 997 - 6345j), 1e-5, 1),
            ((1, 1, 3), 1e-5, 1),
            ((-942848, -942848, 3), 1e-5, 1),
            ((7648, 78378, 7876), 1e-5, 1),
            ((7648, 7648, 7648), 1e-5, 1),
            # ((1_000_001, 10_000_002, 10_000_003), 10, 1)
            ((0, 0, 0), 1e-5, 1),
            ((0, 0, -100.25), 1e-5, 1),
            ((0, 0, 7.31), 1e-5, 1),
            ((0, 4.5, -1.1), 1e-5, 1),
            ((-171.37, -1j, 1j), 1e-5, 1),
            ((0.375, -2j, 2j), 1e-5, 1),
            ((1, -2j, 2j), 1e-5, 1),
            ((2, 2, -7.37), 1e-5, 1),
            ((-2, -2, -0.003), 1e-5, 1),
            ((2, 2, 0.003), 1e-5, 1),
            ((0, -1000, -1000), 1e-5, 1),
            ((1.1, 2.2, 3.3), 1e-5, 1),
            ((-1.1, -2.2, -3.3), 1e-5, 1),
            ((1.11, -1.11, 100_000), 1e-5, 1),
            ((1.11, -1.11, -100_000), 1e-5, 1)
        ]

        for case in test_cases:
            roots, eps, delta = case
            real_roots = list(filter(lambda x: type(x) is not complex, set(roots)))
            a, b, c = get_abc(*roots)

            def f(x: float):
                return x ** 3 + a * x ** 2 + b * x + c

            got_roots = solve_cubic(a, b, c, eps, delta)

            print('want roots:', sorted(real_roots), 'got roots:', sorted(got_roots))

            self.assertEqual(len(real_roots), len(got_roots), 'wrong number of roots')

            for root in got_roots:
                self.assertLessEqual(abs(f(root)), eps)
