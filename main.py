from typing import Callable, Union, Tuple
from sys import argv


def localize(
        f: Callable[[float], float],
        a: float,
        delta: float) -> (float, float):
    if delta == 0:
        raise ValueError('delta cannot be zero')

    if f(a) * f(a + delta) <= 0:
        return a, a + delta

    i: int = 1
    prev_val: float = f(a)

    while True:
        next_val: float = f(a + delta * 2 ** i)
        if prev_val * next_val <= 0:
            return (a + delta * 2 ** (i - 1), a + delta * 2 ** i) if delta > 0 \
                else (a + delta * 2 ** i, a + delta * 2 ** (i - 1))

        prev_val = next_val
        i += 1


def solve_bisect(
        f: Callable[[float], float],
        a: float,
        b: float,
        eps: float) -> float:
    if abs(f(a)) <= eps:
        return a
    if abs(f(b)) <= eps:
        return b

    if f(a) < 0 and f(b) > 0:
        while True:
            c = (a + b) / 2
            f_c = f(c)

            if c == a or c == b:
                print('warning: avoided infinite loop, resulting precision may be lower than requested')
                return c
            if abs(f_c) <= eps:
                return c
            if f_c > eps:
                b = c
            if f_c < -eps:
                a = c

    if f(a) > 0 and f(b) < 0:
        while True:
            c = (a + b) / 2
            f_c = f(c)

            if c == a or c == b:
                print('warning: avoided infinite loop, resulting precision may be lower than requested')
                return c
            if abs(f_c) <= eps or c == a or c == b:
                return c
            if f_c > eps:
                a = c
            if f_c < -eps:
                b = c

    raise ValueError('bad function')


def solve_quadratic(
        a: float,
        b: float,
        c: float) -> Union[Tuple[float, float], Tuple[float], None]:
    if a == 0.0:
        return (-c / b,) if c != 0 else None

    d_thresh = 1e-10
    d = b ** 2 - 4 * a * c

    if d < -d_thresh:
        return None

    if abs(d) <= d_thresh:
        return -b / (2 * a),

    return (-b - d ** 0.5) / (2 * a), (-b + d ** 0.5) / (2 * a)


def solve_cubic(
        a: float,
        b: float,
        c: float,
        eps: float,
        delta: float) -> Union[Tuple[float, float, float], Tuple[float, float], Tuple[float]]:
    """
    Solve a cubic equation of the form x^3 + ax^2 + bx + c = 0
    :return: a tuple containing approximations of equation roots
    """
    derivative_roots = solve_quadratic(3, 2 * a, b)

    def f(x: float):
        return x ** 3 + a * x ** 2 + b * x + c

    if not derivative_roots or len(derivative_roots) == 1:  # single root
        delta = delta if f(0) < 0 else -delta
        x1_interval = localize(f, 0, delta)
        return solve_bisect(f, *x1_interval, eps),

    else:  # many cases
        alpha, beta = derivative_roots
        f_alpha = f(alpha)
        f_beta = f(beta)

        if f_alpha < -eps and f_beta < -eps:
            x1_interval = localize(f, beta, delta)
            return solve_bisect(f, *x1_interval, eps),

        elif abs(f_alpha) <= eps and f_beta < -eps:
            x1: float = alpha
            x2_interval = localize(f, beta, delta)
            return x1, solve_bisect(f, *x2_interval, eps)

        elif f_alpha > eps and f_beta < -eps:
            x1_interval = localize(f, alpha, -delta)
            x2_interval = (alpha, beta)
            x3_interval = localize(f, beta, delta)

            return (solve_bisect(f, *x1_interval, eps), solve_bisect(f, *x2_interval, eps),
                    solve_bisect(f, *x3_interval, eps))

        elif f_alpha > eps >= abs(f_beta):
            x1_interval = localize(f, alpha, -delta)
            x2: float = beta

            return solve_bisect(f, *x1_interval, eps), x2

        elif f_alpha > eps and f_beta > eps:
            x1_interval = localize(f, alpha, -delta)
            return solve_bisect(f, *x1_interval, eps),

        else:  # abs(f_alpha) <= eps and abs(f_beta) <= eps
            return (alpha + beta) / 2,


def main():
    if len(argv) != 6:
        print('bad number of arguments')
        print('usage: python main.py {eps} {delta} {a} {b} {c}')
        return

    eps = float(argv[1])
    delta = float(argv[2])
    a = float(argv[3])
    b = float(argv[4])
    c = float(argv[5])

    roots = solve_cubic(a, b, c, eps, delta)
    print(len(roots), 'root' if len(roots) == 1 else 'roots')
    for i, root in enumerate(roots):
        print('x{i} = {x}'.format(i=i, x=root))


if __name__ == '__main__':
    main()
