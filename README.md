# cubic-equation-solver

Solves cubic equations

# Usage

    $ python main.py {eps} {delta} {a} {b} {c}

### Params

 * a, b, c - coefficients of the cubic function defined by the left-hand side of the equation `x^3 + ax^2 + bx + c = 0`
 * eps - precision level 
 * delta - seek step used when localizing roots
 
# Example

    $ python ./main.py 1e-30 1 -6 11 -6
    
### Output
    
    3 roots
    x0 = 0.9999999999999998
    x1 = 2.0
    x2 = 2.999999999999996

# Requirements 

 * Python 3.8 (could work with older versions idk)
